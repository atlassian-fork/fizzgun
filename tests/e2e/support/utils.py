import contextlib
import io
import os
import sys
import tempfile

from fizzgun.bin import cli


@contextlib.contextmanager
def capture_output(out, err):
    orig_out, orig_err = sys.stdout, sys.stderr
    try:
        sys.stdout, sys.stderr = out, err
        yield
    finally:
        sys.stdout, sys.stderr = orig_out, orig_err


def run_fizzgun_command(*args):
    out = io.StringIO()
    err = io.StringIO()

    with capture_output(out, err):
        cli.main(args=args)
    out, err = out.getvalue(), err.getvalue()

    return out, err


def unique_config_file_name():
    return os.path.join(tempfile.TemporaryDirectory().name, 'config.yaml')
