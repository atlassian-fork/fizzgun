from fizzgun.bubbles import Bubble
from fizzgun.models import Expectations

from tests.e2e.support.utils import run_fizzgun_command
from tests.e2e.support.config_builder import ConfigBuilder


def test_should_output_bubbles_information():
    out, err = run_fizzgun_command('bubbles')

    assert 'Name:' in out
    assert 'Description:' in out
    assert 'Tags:' in out
    assert not err


def test_should_output_custom_bubbles_information_related_to_config_file():
    config = (
        ConfigBuilder()
        .with_bubble_pack({'module': __name__, 'settings': {'DummyBubble': {'expected_magic_number': 42}}})
        .as_tempfile
    )
    out, err = run_fizzgun_command('bubbles', '-c', config)

    assert out == "Name: DummyBubble\n" \
                  "Description: Dummy bubble description\n" \
                  "Tags:\n" \
                  "  * tag:1\n" \
                  "  * tag:2\n" \
                  "Expectations:\n" \
                  "  * Expecting 'status' to be equal to [42]\n" \
                  "  * Expecting 'body' to be equal to ['foo']\n\n"


class DummyBubble(Bubble):
    """Dummy bubble description"""
    TAGS = ['tag:1', 'tag:2']

    def initialize(self, expected_magic_number=200, *args, **kwargs):
        super(DummyBubble, self).initialize(*args, **kwargs)
        self._expectations = Expectations()
        self._expectations.expect('status').to.be_equal_to(expected_magic_number)
        self._expectations.expect('body').to.be_equal_to('foo')


BUBBLES = [DummyBubble]
