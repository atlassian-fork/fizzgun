from tests.e2e.support.utils import run_fizzgun_command, unique_config_file_name


def test_generate_sample_config():
    filename = unique_config_file_name()
    out, err = run_fizzgun_command('gen-config', '-f', filename)

    assert out == 'File generated at: %s\n' % filename
    assert not err

    with open(filename) as fh:
        content = fh.read()

    assert "- endpoint: '.*\.example\.com:(80|443)" in content


def test_generate_config_with_defaults():
    filename = unique_config_file_name()
    out, err = run_fizzgun_command('gen-config', '--defaults', '-f', filename)

    assert out == 'File generated at: %s\n' % filename
    assert not err

    with open(filename) as fh:
        content = fh.read()

    assert 'filters: []' in content
