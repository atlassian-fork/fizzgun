import pytest

from tests.unit.support.fixtures.requests import a_request_with_query, a_www_form_urlencoded_request


form_urlencoded_or_query = pytest.mark.parametrize(
    'request_with_params,params_in_mutant',
    [
        (a_request_with_query, lambda mutation: mutation.query_args),
        (a_www_form_urlencoded_request, lambda mutation: mutation.form_urlencoded_body)
    ],
    ids=['query_string', 'urlencoded_body']
)
