from fizzgun.core.common import Input, Output, ControlMessage


class MockInput(Input):

    def __init__(self, work_items):
        self._items = list(work_items)
        self._items.append(ControlMessage.STOP)

    def pull(self):
        if any(self._items):
            return self._items.pop(0)


class MockOutput(Output):

    def __init__(self):
        self._results = []

    def push(self, result):
        self._results.append(result)

    @property
    def last(self):
        return self._results[-1]

    @property
    def results(self):
        return self._results
