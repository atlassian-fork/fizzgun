import unittest.mock

from fizzgun.application.dependencies import StdoutWriter


class MockStdoutWriter(StdoutWriter):

    def write(self, *args):
        pass

    def get_all_output(self) -> str:
        pass


def create_mock_stdout_writer() -> MockStdoutWriter:
    mock_stdout_writer = unittest.mock.create_autospec(spec=StdoutWriter, instance=True)

    def get_all_output() -> str:
        return '\n'.join(' '.join(call[1]) for call in mock_stdout_writer.write.mock_calls)

    setattr(mock_stdout_writer,
            MockStdoutWriter.get_all_output.__name__, get_all_output)

    return mock_stdout_writer
