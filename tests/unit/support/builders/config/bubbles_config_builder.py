import copy

from fizzgun.config.defaults import defaults


class BubblesConfigBuilder(object):

    def __init__(self, initial=None):
        self._cfg = initial or defaults()['bubbles']

    def with_tags_whitelist(self, *tags: str) -> 'BubblesConfigBuilder':
        cfg = self._copy
        cfg['tags-whitelist'] = list(tags)
        return BubblesConfigBuilder(cfg)

    def with_tags_blacklist(self, *tags: str) -> 'BubblesConfigBuilder':
        cfg = self._copy
        cfg['tags-blacklist'] = list(tags)
        return BubblesConfigBuilder(cfg)

    def with_default_settings(self, **kwargs) -> 'BubblesConfigBuilder':
        cfg = self._copy
        cfg['default-settings'] = kwargs
        return BubblesConfigBuilder(cfg)

    def with_expected_status_range(self, status_range: str) -> 'BubblesConfigBuilder':
        cfg = self._copy
        cfg['default-settings']['expected_status_range'] = status_range
        return BubblesConfigBuilder(cfg)

    def with_mark_requests_enabled(self) -> 'BubblesConfigBuilder':
        cfg = self._copy
        cfg['default-settings']['mark_requests'] = True
        return BubblesConfigBuilder(cfg)

    def with_mark_requests_disabled(self) -> 'BubblesConfigBuilder':
        cfg = self._copy
        cfg['default-settings']['mark_requests'] = False
        return BubblesConfigBuilder(cfg)

    def with_bubble_packs(self, *bubble_packs) -> 'BubblesConfigBuilder':
        cfg = self._copy
        cfg['bubble-packs'] = list(bubble_packs)
        return BubblesConfigBuilder(cfg)

    def build(self):
        return self._copy

    @property
    def _copy(self):
        return copy.deepcopy(self._cfg)
