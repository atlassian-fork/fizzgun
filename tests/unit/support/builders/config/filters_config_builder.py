from typing import List
import copy

from fizzgun.config.defaults import defaults


class FiltersConfigBuilder(object):

    def __init__(self, initial=None):
        self._cfg = initial or defaults()['filters']

    def with_endpoint_rules(self, endpoint_matcher: str, **matching_rules) -> 'FiltersConfigBuilder':
        entry = {'endpoint': endpoint_matcher}
        if matching_rules:
            entry['matchers'] = matching_rules
        cfg = self._copy
        cfg.append(entry)
        return FiltersConfigBuilder(initial=cfg)

    def build(self) -> List:
        return self._copy

    @property
    def _copy(self):
        return copy.deepcopy(self._cfg)
