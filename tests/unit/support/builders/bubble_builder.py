from typing import Type
import copy

from fizzgun.bubbles import Bubble
from fizzgun.models.expectation import Expectations


class MethodCalls(object):
    def __init__(self):
        self.calls = []

    @property
    def call_count(self):
        return len(self.calls)


class MockBubbleWrapper(object):
    def __init__(self, name, doc, tags, generate_yields, does_apply_returns, on_initialize):
        self._name = name
        self._doc = doc
        self._tags = tags
        self._does_apply_return_value = does_apply_returns
        self._yields = generate_yields
        self._initialize_calls = MethodCalls()
        self._does_apply_calls = MethodCalls()
        self._generate_calls = MethodCalls()
        self._on_initialize = on_initialize
        self._wrapped_bubble = self._create_wrapped_bubble()

    @property
    def initialize(self) -> MethodCalls:
        return self._initialize_calls

    @property
    def does_apply(self) -> MethodCalls:
        return self._does_apply_calls

    @property
    def generate(self) -> MethodCalls:
        return self._generate_calls

    @property
    def bubble_class(self) -> Type[Bubble]:
        return self._wrapped_bubble

    def _create_wrapped_bubble(self_wrapper) -> Type[Bubble]:
        class MockBubble(Bubble):
            def initialize(self, **kwargs):
                self_wrapper.initialize.calls.append(kwargs)
                super(MockBubble, self).initialize(**kwargs)
                self_wrapper._on_initialize and self_wrapper._on_initialize(self)

            def does_apply(self, *args):
                self_wrapper.does_apply.calls.append(args)
                if self_wrapper._does_apply_return_value is None:
                    return super(MockBubble, self).does_apply(*args)
                return self_wrapper._does_apply_return_value

            def generate(self, *args):
                self_wrapper.generate.calls.append(args)
                return (item for item in self_wrapper._yields)
        properties = {'TAGS': self_wrapper._tags, '__doc__': self_wrapper._doc}
        return type(self_wrapper._name, (MockBubble,), properties)


class BubbleBuilder(object):
    def __init__(self):
        self._name = 'DefaultTestBubbleName'
        self._tags = []
        self._generate_yields = []
        self._does_apply_returns = None
        self._docstring = None
        self._on_initialize = None

    def with_tags(self, *tags) -> 'BubbleBuilder':
        builder = copy.deepcopy(self)
        builder._tags = list(tags)
        return builder

    def with_name(self, name) -> 'BubbleBuilder':
        builder = copy.deepcopy(self)
        builder._name = name
        return builder

    def with_docstring(self, doc) -> 'BubbleBuilder':
        builder = copy.deepcopy(self)
        builder._docstring = doc
        return builder

    def with_does_apply_return_value(self, value: bool) -> 'BubbleBuilder':
        builder = copy.deepcopy(self)
        builder._does_apply_returns = value
        return builder

    def with_yield_mutations(self, *mutations) -> 'BubbleBuilder':
        builder = copy.deepcopy(self)
        builder._generate_yields = list(mutations)
        return builder

    def with_initialize_callback(self, callback) -> 'BubbleBuilder':
        builder = copy.deepcopy(self)
        builder._on_initialize = callback
        return builder

    def with_expectations(self, expectations: Expectations) -> 'BubbleBuilder':
        def set_expectations(bubble):
            bubble._expectations = expectations
        return self.with_initialize_callback(set_expectations)

    def build(self) -> MockBubbleWrapper:
        return MockBubbleWrapper(self._name,
                                 self._docstring,
                                 copy.deepcopy(self._tags),
                                 copy.deepcopy(self._generate_yields),
                                 self._does_apply_returns,
                                 self._on_initialize)
