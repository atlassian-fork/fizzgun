import copy
import collections
from typing import List, AnyStr, Dict, Tuple, Type

from fizzgun.bin.cli_commands import CmdBubbles
from fizzgun.application.service_contexts import CmdBubblesContext
from fizzgun.bubbles import Bubble

from tests.unit.support.builders.config import ConfigBuilder, BubblesConfigBuilder
from tests.unit.support.mocks import (MockModuleImporter, MockStdoutWriter, MockFileSystem, MockFactory)


CmdOptions = collections.namedtuple('CmdOptions', ['config'])


class CmdBubblesScenarioBuilder(object):

    def __init__(self):
        self._bubbles_config_builder = None
        self._mock_module_importer = None
        self._mock_stdout_writer = None
        self._mock_file_system = None
        self._config_path = None
        self._bubble_packs = []

    def with_mock_module_importer(self, module_importer: MockModuleImporter) -> 'CmdBubblesScenarioBuilder':
        return self._copy_with(mock_module_importer=module_importer)

    def with_mock_sdtoud_writer(self, stdout_writer: MockStdoutWriter) -> 'CmdBubblesScenarioBuilder':
        return self._copy_with(mock_stdout_writer=stdout_writer)

    def with_mock_file_system(self, file_system: MockFileSystem) -> 'CmdBubblesScenarioBuilder':
        return self._copy_with(mock_file_system=file_system)

    def with_bubbles_config_builder(self, bubbles_config_builder: BubblesConfigBuilder) -> 'CmdBubblesScenarioBuilder':
        return self._copy_with(bubbles_config_builder=bubbles_config_builder)

    def given_config_path_is_passed(self, path: str) -> 'CmdBubblesScenarioBuilder':
        return self._copy_with(config_path=path)

    def given_bubbles_are_loaded(self, *bubble_classes) -> 'CmdBubblesScenarioBuilder':
        return self.given_bubble_packs_defined(('default.bubblepack', list(bubble_classes), None))

    def given_bubble_is_loaded(self, bubble_class, module_name=None, settings=None) -> 'CmdBubblesScenarioBuilder':
        module_name = module_name or 'default.bubblepack'
        if settings:
            settings = {bubble_class.__name__: settings}

        return self.given_bubble_packs_defined((module_name, [bubble_class], settings))

    def given_bubble_packs_defined(
            self, *bubble_packs: Tuple[AnyStr, List[Type[Bubble]], Dict or None])-> 'CmdBubblesScenarioBuilder':
        return self._copy_with(bubble_packs=list(bubble_packs), config_path='/default/custom/config.yaml')

    def when_cmd_bubbles_is_executed(self) -> str:
        mock_module_importer = self._mock_module_importer or MockFactory.create_mock_module_importer()
        mock_file_system = self._mock_file_system or MockFactory.create_mock_file_system()
        mock_stdout_writer = self._mock_stdout_writer or MockFactory.create_mock_stdout_writer()
        bubbles_config_builder = self._bubbles_config_builder or BubblesConfigBuilder()

        bubbles_config_builder = self._prepare_bubble_packs(mock_module_importer, bubbles_config_builder)
        bubbles_config = bubbles_config_builder.build()

        config = ConfigBuilder().with_bubbles_config(bubbles_config).to_yaml()
        mock_file_system.given_read_file_returns(config)

        context = CmdBubblesContext(file_system=mock_file_system,
                                    stdout_writer=mock_stdout_writer,
                                    module_importer=mock_module_importer)

        CmdBubbles(context).run(CmdOptions(config=self._config_path))

        return mock_stdout_writer.get_all_output()

    def _prepare_bubble_packs(
            self, mock_module_importer: MockModuleImporter, bubble_config_builder: BubblesConfigBuilder
    ) -> BubblesConfigBuilder:

        if not self._bubble_packs:
            return bubble_config_builder

        config_packs = []
        for module_name, bubble_classes, settings in self._bubble_packs:
            module_conf = {'module': module_name}
            if settings:
                module_conf['settings'] = settings
            config_packs.append(module_conf)

            mock_module_importer.given_module_with_bubbles_exists(module_name, *bubble_classes)
        return bubble_config_builder.with_bubble_packs(*config_packs)

    def _copy_with(self, **kwargs) -> 'CmdBubblesScenarioBuilder':
        new = CmdBubblesScenarioBuilder()
        new._bubbles_config_builder = kwargs.get('bubbles_config_builder', self._bubbles_config_builder)
        new._config_path = kwargs.get('config_path', self._config_path)
        new._mock_module_importer = kwargs.get('mock_module_importer', self._mock_module_importer)
        new._mock_stdout_writer = kwargs.get('mock_stdout_writer', self._mock_stdout_writer)
        new._mock_file_system = kwargs.get('mock_file_system', self._mock_file_system)
        new._bubble_packs = copy.deepcopy(kwargs.get('bubble_packs', self._bubble_packs))
        return new
