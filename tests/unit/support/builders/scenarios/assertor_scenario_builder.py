import copy
from typing import List, Dict, Tuple

from fizzgun.application.service_contexts import ServiceContext
from fizzgun.core import Assertor

from tests.unit.support.mocks.mock_connectors import MockInput, MockOutput


class AssertorScenarioBuilder(object):
    def __init__(self):
        self._incoming_messages = []
        self._expectations = None
        self._response = None
        self._extra_properties = None

    def given_message_contains_expectations(self, expectations: List[Dict]) -> 'AssertorScenarioBuilder':
        return self._copy_with(expectations=expectations)

    def given_message_contains_http_response(self, http_response: Dict) -> 'AssertorScenarioBuilder':
        return self._copy_with(response=http_response)

    def given_message_contains_extra_properties(self, **kwargs) -> 'AssertorScenarioBuilder':
        return self._copy_with(extra_properties=kwargs)

    def given_incoming_messages(self, *incoming_messages: Tuple[List[Dict], Dict]) -> 'AssertorScenarioBuilder':
        return self._copy_with(
            incoming_messages=[{'expectations': m[0], 'response': m[1]} for m in incoming_messages]
        )

    def when_assertor_is_executed(self) -> MockOutput or Exception:
        if self._expectations is not None and self._response:
            message = {'expectations': self._expectations, 'response': self._response}
            message.update(self._extra_properties or {})
            incoming_messages = [message]
        else:
            incoming_messages = copy.deepcopy(self._incoming_messages)

        inputq = MockInput(incoming_messages)
        outputq = MockOutput()

        Assertor(ServiceContext(), inputq, outputq).start()

        return outputq

    def _copy_with(self, **kwargs) -> 'AssertorScenarioBuilder':
        new = AssertorScenarioBuilder()
        new._incoming_messages = copy.deepcopy(kwargs.get('incoming_messages', self._incoming_messages))
        new._expectations = copy.deepcopy(kwargs.get('expectations', self._expectations))
        new._response = copy.deepcopy(kwargs.get('response', self._response))
        new._extra_properties = copy.deepcopy(kwargs.get('extra_properties', self._extra_properties))
        return new
