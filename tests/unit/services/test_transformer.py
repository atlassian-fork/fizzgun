from fizzgun.fizzgun_exception import FizzgunException
from fizzgun.models import HttpRequest
from tests.unit.support.builders.bubble_builder import BubbleBuilder
from tests.unit.support.builders.config import BubblesConfigBuilder
from tests.unit.support.builders.request_builder import RequestBuilder
from tests.unit.support.builders.scenarios import TransformerScenarioBuilder
from tests.unit.support.mocks.mock_id_generator import MockIdGenerator
from tests.unit.support.mocks.mock_module_importer import MockModuleImporter
from tests.unit.support.mocks.mock_stdout_writer import MockStdoutWriter


def test_should_load_fizzgun_bubbles_by_default(
    transformer_scenario_builder: TransformerScenarioBuilder,
    mock_module_importer: MockModuleImporter
):
    (transformer_scenario_builder
     .with_mock_module_importer(mock_module_importer)
     .when_transformer_is_executed())

    mock_module_importer.import_module.assert_called_once_with('fizzgun.bubbles')


def test_should_load_a_different_bubble_pack_when_configured(
    transformer_scenario_builder: TransformerScenarioBuilder,
    mock_module_importer: MockModuleImporter
):
    (transformer_scenario_builder
     .with_mock_module_importer(mock_module_importer)
     .given_bubble_packs_defined(
        ('custom.bubblepack', [], None)
     )
     .when_transformer_is_executed())

    mock_module_importer.import_module.assert_called_once_with('custom.bubblepack')


def test_should_allow_loading_multiple_bubble_packs(
    transformer_scenario_builder: TransformerScenarioBuilder,
    mock_module_importer: MockModuleImporter
):
    (transformer_scenario_builder
     .with_mock_module_importer(mock_module_importer)
     .given_bubble_packs_defined(
        ('custom.bubblepack', [], None),
        ('another.bubblepack', [], None)
     )
     .when_transformer_is_executed())

    assert mock_module_importer.import_module.call_count == 2
    mock_module_importer.import_module.assert_any_call('custom.bubblepack')
    mock_module_importer.import_module.assert_any_call('another.bubblepack')


def test_should_raise_the_error_when_failing_to_import_a_bubble_package(
    transformer_scenario_builder: TransformerScenarioBuilder,
    mock_module_importer: MockModuleImporter
):
    cause = Exception('oops')

    mock_module_importer.given_import_modules_raises_error(cause)
    result = (
        transformer_scenario_builder
        .with_mock_module_importer(mock_module_importer)
        .with_bubbles_config_builder(BubblesConfigBuilder().with_bubble_packs({'module': 'test.package'}))
        .when_transformer_is_executed())
    assert isinstance(result, FizzgunException)
    assert result.message == "Unable to import bubble package 'test.package': oops"
    assert result.reason == cause


def test_should_raise_the_error_if_imported_bubble_package_has_no_expected_bubbles_attribute(
    transformer_scenario_builder: TransformerScenarioBuilder,
    mock_module_importer: MockModuleImporter
):
    mock_module_importer.given_module_with_missing_bubble_attribute_exists('test.package')
    result = (
        transformer_scenario_builder
        .with_mock_module_importer(mock_module_importer)
        .with_bubbles_config_builder(BubblesConfigBuilder().with_bubble_packs({'module': 'test.package'}))
        .when_transformer_is_executed())

    assert isinstance(result, FizzgunException)
    assert result.message == "Bubble package 'test.package' has no 'BUBBLES' attribute"
    assert result.reason is None


def test_should_display_the_name_of_the_loaded_bubble(
    transformer_scenario_builder: TransformerScenarioBuilder,
    mock_stdout_writer: MockStdoutWriter
):
    (
        transformer_scenario_builder
        .with_mock_sdtoud_writer(mock_stdout_writer)
        .given_bubble_is_loaded(BubbleBuilder().with_name('MyBubble').build().bubble_class)
        .when_transformer_is_executed()
    )
    mock_stdout_writer.write.assert_called_once_with('Loading bubble MyBubble')


def test_should_invoke_every_loaded_bubble_for_each_input(transformer_scenario_builder: TransformerScenarioBuilder):
    bubble_1 = BubbleBuilder().build()
    bubble_2 = BubbleBuilder().build()

    (
        transformer_scenario_builder
        .given_bubbles_are_loaded(bubble_1.bubble_class, bubble_2.bubble_class)
        .given_incoming_requests(
            an_http_request(),
            an_http_request())
        .when_transformer_is_executed()
    )

    assert bubble_1.generate.call_count == 2
    assert bubble_2.generate.call_count == 2


def test_should_pass_the_incoming_request_as_an_http_request_entity(
    transformer_scenario_builder: TransformerScenarioBuilder
):
    bubble = BubbleBuilder().build()
    incoming_message = an_http_request()

    (
        transformer_scenario_builder
        .given_bubble_is_loaded(bubble.bubble_class)
        .given_incoming_requests(incoming_message)
        .when_transformer_is_executed()
    )

    assert bubble.generate.call_count == 1
    assert isinstance(bubble.generate.calls[0][0], HttpRequest)
    assert bubble.generate.calls[0][0].value == incoming_message


def test_should_invoke_does_apply_method_in_bubble_with_the_incoming_request(
    transformer_scenario_builder: TransformerScenarioBuilder
):
    bubble = BubbleBuilder().build()
    incoming_message = an_http_request()

    (
        transformer_scenario_builder
        .given_bubble_is_loaded(bubble.bubble_class)
        .given_incoming_requests(incoming_message)
        .when_transformer_is_executed()
    )

    assert bubble.does_apply.call_count == 1
    assert isinstance(bubble.does_apply.calls[0][0], HttpRequest)
    assert bubble.does_apply.calls[0][0].value == incoming_message


def test_should_call_generate_when_request_applies_to_bubble(
    transformer_scenario_builder: TransformerScenarioBuilder
):
    bubble = BubbleBuilder().with_does_apply_return_value(True).build()

    (
        transformer_scenario_builder
        .given_bubble_is_loaded(bubble.bubble_class)
        .given_incoming_requests(an_http_request())
        .when_transformer_is_executed()
    )

    assert bubble.does_apply.call_count == 1
    assert bubble.generate.call_count == 1


def test_should_not_call_generate_when_request_does_not_apply_to_bubble(
    transformer_scenario_builder: TransformerScenarioBuilder
):
    bubble = BubbleBuilder().with_does_apply_return_value(False).build()

    (
        transformer_scenario_builder
        .given_bubble_is_loaded(bubble.bubble_class)
        .given_incoming_requests(an_http_request())
        .when_transformer_is_executed()
    )

    assert bubble.does_apply.call_count == 1
    assert bubble.generate.call_count == 0


def test_should_collect_all_the_generated_transformations(
    transformer_scenario_builder: TransformerScenarioBuilder
):
    original = an_http_request(path='/original/request')
    mutation1 = an_http_request(path='/modification/1')
    mutation2 = an_http_request(path='/modification/2')
    mutation3 = an_http_request(path='/modification/3')

    bubble1 = BubbleBuilder().with_yield_mutations(mutation1, mutation2).build()
    bubble2 = BubbleBuilder().with_yield_mutations(mutation3).build()

    output = (
        transformer_scenario_builder
        .given_bubbles_are_loaded(bubble1.bubble_class, bubble2.bubble_class)
        .given_incoming_requests(original)
        .when_transformer_is_executed()
    )

    assert len(output.results) == 3
    assert all(result['original'] == original for result in output.results)
    assert any(result['modified'] == mutation1 for result in output.results)
    assert any(result['modified'] == mutation2 for result in output.results)
    assert any(result['modified'] == mutation3 for result in output.results)


def test_yielded_mutations_should_contain_info_of_bubble_that_generated_them(
    transformer_scenario_builder: TransformerScenarioBuilder
):
    bubble = (
        BubbleBuilder()
        .with_name('MyBubble')
        .with_docstring('the description from the doc')
        .with_yield_mutations(an_http_request())
        .build()
    )

    output = (
        transformer_scenario_builder
        .given_bubble_is_loaded(bubble.bubble_class)
        .given_incoming_requests(an_http_request())
        .when_transformer_is_executed()
    )

    assert output.last['bubble'] == {'name': 'MyBubble', 'description': 'the description from the doc'}


def test_yielded_mutations_should_contain_default_response_expectations_on_status_code(
    transformer_scenario_builder: TransformerScenarioBuilder
):
    bubble = (
        BubbleBuilder()
        .with_yield_mutations(an_http_request())
        .build()
    )

    output = (
        transformer_scenario_builder
        .given_bubble_is_loaded(bubble.bubble_class)
        .given_incoming_requests(an_http_request())
        .when_transformer_is_executed()
    )

    assert output.last['expectations'] == [{'field': 'status', 'op': 'be_in_ranges', 'args': ['0-499']}]


def test_should_allow_overriding_default_status_code_expectation_for_all_bubbles(
    transformer_scenario_builder: TransformerScenarioBuilder
):
    bubble = (
        BubbleBuilder()
        .with_yield_mutations(an_http_request())
        .build()
    )

    output = (
        transformer_scenario_builder
        .with_bubbles_config_builder(
            BubblesConfigBuilder().with_expected_status_range('100-399')
        )
        .given_bubble_is_loaded(bubble.bubble_class)
        .given_incoming_requests(an_http_request())
        .when_transformer_is_executed()
    )

    assert output.last['expectations'] == [{'field': 'status', 'op': 'be_in_ranges', 'args': ['100-399']}]


def test_should_allow_overriding_default_status_code_expectation_per_bubble(
    transformer_scenario_builder: TransformerScenarioBuilder
):
    bubble1 = (
        BubbleBuilder()
        .with_name('WithSettings')
        .with_yield_mutations(an_http_request())
        .build()
    )

    bubble2 = (
        BubbleBuilder()
        .with_name('WithoutSettings')
        .with_yield_mutations(an_http_request())
        .build()
    )

    bubble_pack_settings = {'WithSettings': {'expected_status_range': '200-217'}}

    output = (
        transformer_scenario_builder
        .with_bubbles_config_builder(
            BubblesConfigBuilder().with_expected_status_range('100-399')
        )
        .given_bubble_packs_defined(
            ('test.bubble-pack', [bubble1.bubble_class, bubble2.bubble_class], bubble_pack_settings)
        )
        .given_incoming_requests(an_http_request())
        .when_transformer_is_executed()
    )

    result1 = next(result for result in output.results if result['bubble']['name'] == 'WithSettings')
    result2 = next(result for result in output.results if result['bubble']['name'] == 'WithoutSettings')

    assert result1['expectations'] == [{'field': 'status', 'op': 'be_in_ranges', 'args': ['200-217']}]
    assert result2['expectations'] == [{'field': 'status', 'op': 'be_in_ranges', 'args': ['100-399']}]


def test_should_allow_bubbles_to_define_their_own_response_expectations(
    transformer_scenario_builder: TransformerScenarioBuilder
):
    bubble = (
        BubbleBuilder()
        .with_initialize_callback(
            lambda bubble_instance: bubble_instance.expectations.expect('body').not_to.include('test')
        )
        .with_yield_mutations(an_http_request())
        .build()
    )
    output = (
        transformer_scenario_builder
        .given_bubble_is_loaded(bubble.bubble_class)
        .given_incoming_requests(an_http_request())
        .when_transformer_is_executed()
    )

    expectations = output.last['expectations']

    assert expectations == [
        {'field': 'status', 'op': 'be_in_ranges', 'args': ['0-499']},
        {'field': 'body', 'not': True, 'op': 'include', 'args': ['test']}
    ]


def test_should_not_mark_mutated_requests_by_default(
    transformer_scenario_builder: TransformerScenarioBuilder
):
    bubble = BubbleBuilder().with_yield_mutations(an_http_request()).build()

    output = (
        transformer_scenario_builder
        .given_bubble_is_loaded(bubble.bubble_class)
        .given_incoming_requests(an_http_request())
        .when_transformer_is_executed()
    )

    mutant_headers = [header[0].lower() for header in output.last['modified']['headers']]
    assert 'x-fizzgun-id' not in mutant_headers


def test_should_mark_mutated_requests_when_mark_request_is_enabled(
    transformer_scenario_builder: TransformerScenarioBuilder,
    mock_id_generator: MockIdGenerator
):
    bubble = BubbleBuilder().with_yield_mutations(an_http_request()).build()

    mock_id_generator.given_generate_returns('a-unique-request-id')

    output = (
        transformer_scenario_builder
        .with_mock_id_generator(mock_id_generator)
        .with_bubbles_config_builder(
            BubblesConfigBuilder().with_mark_requests_enabled()
        )
        .given_bubble_is_loaded(bubble.bubble_class)
        .given_incoming_requests(an_http_request())
        .when_transformer_is_executed()
    )

    mutant_headers = [(header[0].lower(), header[1]) for header in output.last['modified']['headers']]
    assert ('x-fizzgun-id', 'a-unique-request-id') in mutant_headers


def test_should_request_a_new_id_for_each_mutated_request(
    transformer_scenario_builder: TransformerScenarioBuilder,
    mock_id_generator: MockIdGenerator
):
    bubble = BubbleBuilder().with_yield_mutations(an_http_request(), an_http_request()).build()
    mock_id_generator.given_generate_returns_unique_ids()

    output = (
        transformer_scenario_builder
        .with_mock_id_generator(mock_id_generator)
        .with_bubbles_config_builder(
            BubblesConfigBuilder().with_mark_requests_enabled()
        )
        .given_bubble_is_loaded(bubble.bubble_class)
        .given_incoming_requests(an_http_request())
        .when_transformer_is_executed()
    )

    fizzgun_ids = [
        next(header[1] for header in result['modified']['headers'] if header[0].lower() == 'x-fizzgun-id')
        for result in output.results
    ]

    assert fizzgun_ids[0]
    assert fizzgun_ids[1]
    assert fizzgun_ids[0] != fizzgun_ids[1]


def test_should_allow_defining_custom_bubble_initialization_parameters(
    transformer_scenario_builder: TransformerScenarioBuilder
):
    bubble = BubbleBuilder().build()

    (
        transformer_scenario_builder
        .given_bubble_is_loaded(bubble.bubble_class, settings={'some_parameter': 1, 'another_param': [2]})
        .when_transformer_is_executed()
    )

    assert bubble.initialize.call_count == 1
    initialize_args = bubble.initialize.calls[0]

    assert initialize_args['some_parameter'] == 1
    assert initialize_args['another_param'] == [2]


def test_should_not_load_bubble_containing_at_least_one_blacklisted_tag(
    transformer_scenario_builder: TransformerScenarioBuilder
):
    bubble = BubbleBuilder().with_tags('include:me', 'blacklist:me', 'include:me:too').build()

    (
        transformer_scenario_builder
        .with_bubbles_config_builder(BubblesConfigBuilder().with_tags_blacklist('blacklist:me', 'also:this'))
        .given_bubble_is_loaded(bubble.bubble_class)
        .when_transformer_is_executed()
    )

    assert bubble.initialize.call_count == 0


def test_should_load_bubble_with_no_tags_if_blacklist_given(
    transformer_scenario_builder: TransformerScenarioBuilder
):
    bubble = BubbleBuilder().build()

    (
        transformer_scenario_builder
        .with_bubbles_config_builder(BubblesConfigBuilder().with_tags_blacklist('blacklist:me'))
        .given_bubble_is_loaded(bubble.bubble_class)
        .when_transformer_is_executed()
    )

    assert bubble.initialize.call_count == 1


def test_should_load_bubble_with_tags_not_in_blacklist(
    transformer_scenario_builder: TransformerScenarioBuilder
):
    bubble = BubbleBuilder().with_tags('tag1', 'tag2').build()

    (
        transformer_scenario_builder
        .with_bubbles_config_builder(BubblesConfigBuilder().with_tags_blacklist('blacklist:me'))
        .given_bubble_is_loaded(bubble.bubble_class)
        .when_transformer_is_executed()
    )

    assert bubble.initialize.call_count == 1


def test_should_not_load_bubble_not_containing_whitelisted_tag(
    transformer_scenario_builder: TransformerScenarioBuilder
):
    bubble = BubbleBuilder().with_tags('tag1', 'tag2').build()

    (
        transformer_scenario_builder
        .with_bubbles_config_builder(BubblesConfigBuilder().with_tags_whitelist('whitelist:me'))
        .given_bubble_is_loaded(bubble.bubble_class)
        .when_transformer_is_executed()
    )

    assert bubble.initialize.call_count == 0


def test_should_load_bubble_containing_at_least_one_whitelisted_tag(
    transformer_scenario_builder: TransformerScenarioBuilder
):
    bubble = BubbleBuilder().with_tags('tag1', 'whitelist:me').build()

    (
        transformer_scenario_builder
        .with_bubbles_config_builder(BubblesConfigBuilder().with_tags_whitelist('whitelist:you', 'whitelist:me'))
        .given_bubble_is_loaded(bubble.bubble_class)
        .when_transformer_is_executed()
    )

    assert bubble.initialize.call_count == 1


def test_should_not_load_bubble_with_no_tags_if_whitelist_given(
    transformer_scenario_builder: TransformerScenarioBuilder
):
    bubble = BubbleBuilder().build()

    (
        transformer_scenario_builder
        .with_bubbles_config_builder(BubblesConfigBuilder().with_tags_whitelist('tag1', 'tag2'))
        .given_bubble_is_loaded(bubble.bubble_class)
        .when_transformer_is_executed()
    )

    assert bubble.initialize.call_count == 0


def test_should_not_load_whitelisted_bubble_if_also_blacklisted(
    transformer_scenario_builder: TransformerScenarioBuilder
):
    bubble = BubbleBuilder().with_tags('whitelist:me', 'blacklist:me').build()

    (
        transformer_scenario_builder
        .with_bubbles_config_builder(
            BubblesConfigBuilder().with_tags_whitelist('whitelist:me').with_tags_blacklist('blacklist:me'))
        .given_bubble_is_loaded(bubble.bubble_class)
        .when_transformer_is_executed()
    )

    assert bubble.initialize.call_count == 0


def test_should_load_whitelisted_bubble_if_not_blacklisted(
    transformer_scenario_builder: TransformerScenarioBuilder
):
    bubble = BubbleBuilder().with_tags('whitelist:me', 'blacklist:other').build()

    (
        transformer_scenario_builder
        .with_bubbles_config_builder(
            BubblesConfigBuilder().with_tags_whitelist('whitelist:me').with_tags_blacklist('blacklist:me'))
        .given_bubble_is_loaded(bubble.bubble_class)
        .when_transformer_is_executed()
    )

    assert bubble.initialize.call_count == 1


def an_http_request(path=None):
    path = path or '/default-path'
    return RequestBuilder.with_path(path).build()
