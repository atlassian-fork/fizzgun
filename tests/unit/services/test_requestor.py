from tests.unit.support.builders.request_builder import RequestBuilder
from tests.unit.support.builders.response_builder import ResponseBuilder
from tests.unit.support.builders.scenarios import RequestorScenarioBuilder
from tests.unit.support.mocks import MockHttpClient


def test_should_invoke_the_http_client_with_the_modified_request_properties(
    requestor_scenario_builder: RequestorScenarioBuilder, mock_http_client: MockHttpClient
):
    (
        requestor_scenario_builder
        .with_mock_http_client(mock_http_client)
        .given_message_contains_modified_request(
            RequestBuilder
            .with_method('PUT')
            .with_url('https://test.com/path/to/resource?query=string')
            .with_headers(('Content-Type', 'text/plain'), ('X-Custom', 'valueA'))
            .with_body('tha body')
            .build()
        )
        .when_requestor_is_executed()
    )

    mock_http_client.request.assert_called_once_with(
        'PUT',
        'https://test.com:443/path/to/resource?query=string',
        {'content-type': 'text/plain', 'x-custom': 'valueA'},
        'tha body'
    )


def test_should_add_the_obtained_response_to_the_output(requestor_scenario_builder: RequestorScenarioBuilder):
    response = (
        ResponseBuilder()
        .with_status(404)
        .with_reason('not found')
        .with_header('try', 'again')
        .with_body('Not Found').build())

    output = (
        requestor_scenario_builder
        .given_http_client_returns_response(response)
        .when_requestor_is_executed()
    )

    assert len(output.results) == 1
    assert output.last['response'] == response


def test_should_include_in_the_output_the_modified_request_and_any_other_properties_from_the_input(
    requestor_scenario_builder: RequestorScenarioBuilder
):
    request = RequestBuilder.build()

    output = (
        requestor_scenario_builder
        .given_message_contains_modified_request(request)
        .given_message_contains_extra_properties(foo='bar')
        .when_requestor_is_executed()
    )

    assert len(output.results) == 1
    assert output.last['modified'] == request
    assert output.last['foo'] == 'bar'
