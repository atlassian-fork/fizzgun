from tests.unit.support.builders.scenarios import ReporterScenarioBuilder
from tests.unit.support.builders.config import ReportConfigBuilder
from tests.unit.support.builders.request_builder import RequestBuilder
from tests.unit.support.builders.response_builder import ResponseBuilder
from tests.unit.support.mocks import MockFileSystem
from tests.unit.support.util import Any, StringMatching, StringContaining


def test_should_create_report_directory_once_when_initialized(
    reporter_scenario_builder: ReporterScenarioBuilder, mock_file_system: MockFileSystem
):
    (
        reporter_scenario_builder
        .with_mock_file_system(mock_file_system)
        .given_message_is_received_times(3)
        .when_reporter_is_executed()
    )
    assert mock_file_system.create_directory.call_count == 1


def test_should_create_report_directory_in_the_default_location_when_no_config_is_set(
    reporter_scenario_builder: ReporterScenarioBuilder, mock_file_system: MockFileSystem
):
    (
        reporter_scenario_builder
        .with_mock_file_system(mock_file_system)
        .when_reporter_is_executed()
    )
    mock_file_system.create_directory.assert_called_once_with('./reports')


def test_should_create_report_directory_in_custom_location_when_set_from_config(
    reporter_scenario_builder: ReporterScenarioBuilder, mock_file_system: MockFileSystem
):
    (
        reporter_scenario_builder
        .with_mock_file_system(mock_file_system)
        .given_report_config(ReportConfigBuilder().with_report_directory('/some/other/place').build())
        .when_reporter_is_executed()
    )
    mock_file_system.create_directory.assert_called_once_with('/some/other/place')


def test_should_create_a_report_with_txt_extension_when_no_config_is_set(
    reporter_scenario_builder: ReporterScenarioBuilder, mock_file_system: MockFileSystem
):
    (
        reporter_scenario_builder
        .with_mock_file_system(mock_file_system)
        .when_reporter_is_executed()
    )
    mock_file_system.append_to_file.assert_called_once_with(StringMatching(r'\.txt$'), Any(str))


def test_should_create_a_report_with_html_extension_when_specified_in_config(
    reporter_scenario_builder: ReporterScenarioBuilder, mock_file_system: MockFileSystem
):
    (
        reporter_scenario_builder
        .with_mock_file_system(mock_file_system)
        .given_report_config(ReportConfigBuilder().with_report_format('html').build())
        .when_reporter_is_executed()
    )
    mock_file_system.append_to_file.assert_called_once_with(StringMatching(r'\.html$'), Any(str))


def test_should_create_a_report_with_txt_extension_when_specified_in_config(
    reporter_scenario_builder: ReporterScenarioBuilder, mock_file_system: MockFileSystem
):
    (
        reporter_scenario_builder
        .with_mock_file_system(mock_file_system)
        .given_report_config(ReportConfigBuilder().with_report_format('txt').build())
        .when_reporter_is_executed()
    )
    mock_file_system.append_to_file.assert_called_once_with(StringMatching(r'\.txt'), Any(str))


def test_should_create_a_report_with_txt_extension_when_invalid_format_is_specified_in_config(
    reporter_scenario_builder: ReporterScenarioBuilder, mock_file_system: MockFileSystem
):
    (
        reporter_scenario_builder
        .with_mock_file_system(mock_file_system)
        .given_report_config(ReportConfigBuilder().with_report_format('bogus').build())
        .when_reporter_is_executed()
    )
    mock_file_system.append_to_file.assert_called_once_with(StringMatching(r'\.txt'), Any(str))


def test_should_generate_a_default_report_base_name_when_no_session_id_in_message(
    reporter_scenario_builder: ReporterScenarioBuilder, mock_file_system: MockFileSystem
):
    (
        reporter_scenario_builder
        .with_mock_file_system(mock_file_system)
        .given_report_config(ReportConfigBuilder().with_report_directory('/test').with_report_format('txt').build())
        .when_reporter_is_executed()
    )
    mock_file_system.append_to_file.assert_called_once_with('/test/global-session.txt', Any(str))


def test_should_use_the_session_id_to_generate_report_base_name_when_specified(
    reporter_scenario_builder: ReporterScenarioBuilder, mock_file_system: MockFileSystem
):
    (
        reporter_scenario_builder
        .with_mock_file_system(mock_file_system)
        .given_report_config(ReportConfigBuilder().with_report_directory('/test').with_report_format('html').build())
        .given_message_contains_session_id('my-cool-id')
        .when_reporter_is_executed()
    )
    mock_file_system.append_to_file.assert_called_once_with('/test/my-cool-id.html', Any(str))


def test_should_normalize_the_base_name_when_session_id_contains_invalid_filename_characters(
    reporter_scenario_builder: ReporterScenarioBuilder, mock_file_system: MockFileSystem
):
    (
        reporter_scenario_builder
        .with_mock_file_system(mock_file_system)
        .given_report_config(ReportConfigBuilder().with_report_directory('/test').with_report_format('txt').build())
        .given_message_contains_session_id('my./cool*-id_129')
        .when_reporter_is_executed()
    )
    mock_file_system.append_to_file.assert_called_once_with('/test/mycool-id_129.txt', Any(str))


def test_should_use_default_report_base_name_when_normalized_string_from_session_id_results_in_empty_string(
    reporter_scenario_builder: ReporterScenarioBuilder, mock_file_system: MockFileSystem
):
    (
        reporter_scenario_builder
        .with_mock_file_system(mock_file_system)
        .given_report_config(ReportConfigBuilder().with_report_directory('/test').with_report_format('txt').build())
        .given_message_contains_session_id('$%^&*)./')
        .when_reporter_is_executed()
    )
    mock_file_system.append_to_file.assert_called_once_with('/test/global-session.txt', Any(str))


def test_should_append_results_to_report_for_each_incoming_message(
    reporter_scenario_builder: ReporterScenarioBuilder, mock_file_system: MockFileSystem
):
    (
        reporter_scenario_builder
        .with_mock_file_system(mock_file_system)
        .given_message_is_received_times(3)
        .when_reporter_is_executed()
    )
    assert mock_file_system.append_to_file.call_count == 3


def test_should_include_bubble_details_in_the_txt_report_result_entry(
    reporter_scenario_builder: ReporterScenarioBuilder, mock_file_system: MockFileSystem
):
    (
        reporter_scenario_builder
        .with_mock_file_system(mock_file_system)
        .given_report_config(ReportConfigBuilder().with_report_format('txt').build())
        .given_message_contains_bubble_info('MagicBubble', 'this bubble does magic')
        .when_reporter_is_executed()
    )
    mock_file_system.append_to_file.assert_called_once_with(Any(str),
                                                            StringContaining("\nBubble: MagicBubble\n"))
    mock_file_system.append_to_file.assert_called_once_with(Any(str),
                                                            StringContaining("\nDescription: this bubble does magic\n"))


def test_should_include_list_of_unmet_expectations_in_the_txt_report_result_entry(
    reporter_scenario_builder: ReporterScenarioBuilder, mock_file_system: MockFileSystem
):
    (
        reporter_scenario_builder
        .with_mock_file_system(mock_file_system)
        .given_report_config(ReportConfigBuilder().with_report_format('txt').build())
        .given_message_contains_error_messages('error 1', 'another error')
        .when_reporter_is_executed()
    )
    mock_file_system.append_to_file.assert_called_once_with(Any(str),
                                                            StringContaining("\nErrors:\n"))
    mock_file_system.append_to_file.assert_called_once_with(Any(str),
                                                            StringContaining("\n    * error 1\n"))
    mock_file_system.append_to_file.assert_called_once_with(Any(str),
                                                            StringContaining("\n    * another error\n"))


def test_should_include_original_request_in_the_txt_report_result_entry(
    reporter_scenario_builder: ReporterScenarioBuilder, mock_file_system: MockFileSystem
):
    (
        reporter_scenario_builder
        .with_mock_file_system(mock_file_system)
        .given_report_config(ReportConfigBuilder().with_report_format('txt').build())
        .given_message_contains_original_request(
            RequestBuilder
            .with_method('POST')
            .with_url('http://example.com/path?query=string')
            .with_http_version('HTTP/1.1')
            .with_header('Content-Type', 'application/json')
            .with_header('Accept-Language', 'es')
            .with_body('{"the": "body"}')
            .build()
        )
        .when_reporter_is_executed()
    )
    mock_file_system.append_to_file.assert_called_once_with(Any(str),
                                                            StringContaining("\n    Original Request:\n"))
    mock_file_system.append_to_file.assert_called_once_with(Any(str),
                                                            StringContaining(
        "\n"
        "POST http://example.com:80/path?query=string HTTP/1.1\n"
        "Content-Type: application/json\n"
        "Accept-Language: es\n"
        "\n"
        '{"the": "body"}\n'
    ))


def test_should_include_modified_request_in_the_txt_report_result_entry(
    reporter_scenario_builder: ReporterScenarioBuilder, mock_file_system: MockFileSystem
):
    (
        reporter_scenario_builder
        .with_mock_file_system(mock_file_system)
        .given_report_config(ReportConfigBuilder().with_report_format('txt').build())
        .given_message_contains_modified_request(
            RequestBuilder
            .with_method('POST')
            .with_url('http://example.com/path?query=string')
            .with_http_version('HTTP/1.1')
            .with_header('Content-Type', 'application/json')
            .with_header('Accept-Language', 'es')
            .with_body('{"the": "body"}')
            .build()
        )
        .when_reporter_is_executed()
    )
    mock_file_system.append_to_file.assert_called_once_with(Any(str),
                                                            StringContaining("\n    Modified Request:\n"))
    mock_file_system.append_to_file.assert_called_once_with(Any(str),
                                                            StringContaining(
        "\n"
        "POST http://example.com:80/path?query=string HTTP/1.1\n"
        "Content-Type: application/json\n"
        "Accept-Language: es\n"
        "\n"
        '{"the": "body"}\n'
    ))


def test_should_include_server_response_in_the_txt_report_result_entry(
    reporter_scenario_builder: ReporterScenarioBuilder, mock_file_system: MockFileSystem
):
    (
        reporter_scenario_builder
        .with_mock_file_system(mock_file_system)
        .given_report_config(ReportConfigBuilder().with_report_format('txt').build())
        .given_message_contains_response(
            ResponseBuilder()
            .with_status(500)
            .with_reason('Internal Server Error')
            .with_header('Content-Type', 'application/json')
            .with_header('Server', 'dummy v1.0')
            .with_body('{"error": "I was not expecting fizzgun"}')
            .build()
        )
        .when_reporter_is_executed()
    )
    mock_file_system.append_to_file.assert_called_once_with(Any(str),
                                                            StringContaining("\n    Server Response:\n"))
    mock_file_system.append_to_file.assert_called_once_with(Any(str),
                                                            StringContaining(
        "\n"
        "500 Internal Server Error\n"
        "Content-Type: application/json\n"
        "Server: dummy v1.0\n"
        "\n"
        '{"error": "I was not expecting fizzgun"}\n'
    ))


def test_should_include_bubble_details_in_the_html_report_result_entry(
    reporter_scenario_builder: ReporterScenarioBuilder, mock_file_system: MockFileSystem
):
    (
        reporter_scenario_builder
        .with_mock_file_system(mock_file_system)
        .given_report_config(ReportConfigBuilder().with_report_format('html').build())
        .given_message_contains_bubble_info('MagicBubble', 'this bubble does magic')
        .when_reporter_is_executed()
    )
    mock_file_system.append_to_file.assert_called_once_with(
        Any(str), StringContaining("<td><b>MagicBubble: </b>this bubble does magic</td>"))


def test_should_include_list_of_unmet_expectations_in_the_html_report_result_entry(
    reporter_scenario_builder: ReporterScenarioBuilder, mock_file_system: MockFileSystem
):
    (
        reporter_scenario_builder
        .with_mock_file_system(mock_file_system)
        .given_report_config(ReportConfigBuilder().with_report_format('html').build())
        .given_message_contains_error_messages('error 1', 'another error')
        .when_reporter_is_executed()
    )

    mock_file_system.append_to_file.assert_called_once_with(Any(str),
                                                            StringContaining("<li>error 1</li>"))
    mock_file_system.append_to_file.assert_called_once_with(Any(str),
                                                            StringContaining("<li>another error</li>"))


def test_should_include_original_request_in_the_html_report_result_entry(
    reporter_scenario_builder: ReporterScenarioBuilder, mock_file_system: MockFileSystem
):
    (
        reporter_scenario_builder
        .with_mock_file_system(mock_file_system)
        .given_report_config(ReportConfigBuilder().with_report_format('html').build())
        .given_message_contains_original_request(
            RequestBuilder
            .with_method('POST')
            .with_url('http://example.com/path?query=string')
            .with_http_version('HTTP/1.1')
            .with_header('Content-Type', 'application/json')
            .with_header('Accept-Language', 'es')
            .with_body('{"the": "body"}')
            .build()
        )
        .when_reporter_is_executed()
    )
    mock_file_system.append_to_file.assert_called_once_with(
        Any(str),
        StringContaining("<b>POST</b> http://example.com:80/path?query=string HTTP/1.1<br>"))

    mock_file_system.append_to_file.assert_called_once_with(
        Any(str),
        StringContaining("<b>Content-Type</b>: application/json<br>"))

    mock_file_system.append_to_file.assert_called_once_with(
        Any(str),
        StringContaining("<b>Accept-Language</b>: es<br>"))

    mock_file_system.append_to_file.assert_called_once_with(Any(str),
                                                            StringContaining('{&quot;the&quot;: &quot;body&quot;}'))


def test_should_include_modified_request_in_the_html_report_result_entry(
    reporter_scenario_builder: ReporterScenarioBuilder, mock_file_system: MockFileSystem
):
    (
        reporter_scenario_builder
        .with_mock_file_system(mock_file_system)
        .given_report_config(ReportConfigBuilder().with_report_format('html').build())
        .given_message_contains_modified_request(
            RequestBuilder
            .with_method('POST')
            .with_url('http://example.com/path?query=string')
            .with_http_version('HTTP/1.1')
            .with_header('Content-Type', 'application/json')
            .with_header('Accept-Language', 'es')
            .with_body('{"the": "body"}')
            .build()
        )
        .when_reporter_is_executed()
    )
    mock_file_system.append_to_file.assert_called_once_with(
        Any(str),
        StringContaining("<b>POST</b> http://example.com:80/path?query=string HTTP/1.1<br>"))

    mock_file_system.append_to_file.assert_called_once_with(
        Any(str),
        StringContaining("<b>Content-Type</b>: application/json<br>"))

    mock_file_system.append_to_file.assert_called_once_with(
        Any(str),
        StringContaining("<b>Accept-Language</b>: es<br>"))

    mock_file_system.append_to_file.assert_called_once_with(Any(str),
                                                            StringContaining('{&quot;the&quot;: &quot;body&quot;}'))


def test_should_include_server_response_in_the_html_report_result_entry(
    reporter_scenario_builder: ReporterScenarioBuilder, mock_file_system: MockFileSystem
):
    (
        reporter_scenario_builder
        .with_mock_file_system(mock_file_system)
        .given_report_config(ReportConfigBuilder().with_report_format('html').build())
        .given_message_contains_response(
            ResponseBuilder()
            .with_status(500)
            .with_reason('Internal Server Error')
            .with_header('Content-Type', 'application/json')
            .with_header('Server', 'dummy v1.0')
            .with_body('{"error": "I was not expecting fizzgun"}')
            .build()
        )
        .when_reporter_is_executed()
    )

    mock_file_system.append_to_file.assert_called_once_with(
        Any(str),
        StringContaining("<b>500 Internal Server Error</b><br>"))

    mock_file_system.append_to_file.assert_called_once_with(
        Any(str),
        StringContaining("<b>Content-Type</b>: application/json<br>"))

    mock_file_system.append_to_file.assert_called_once_with(
        Any(str),
        StringContaining("<b>Server</b>: dummy v1.0<br>"))

    mock_file_system.append_to_file.assert_called_once_with(
        Any(str),
        StringContaining('{&quot;error&quot;: &quot;I was not expecting fizzgun&quot;}'))
