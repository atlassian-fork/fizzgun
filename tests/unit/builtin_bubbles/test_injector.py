import pytest

from tests.unit.support.builders.scenarios.bubble_scenario_builder import BubbleScenarioBuilder
from tests.unit.support.fixtures.parametrize_tests import form_urlencoded_or_query
from tests.unit.support.fixtures.requests import a_json_request


@pytest.fixture()
def injector_scenario() -> BubbleScenarioBuilder:
    return BubbleScenarioBuilder('fizzgun.bubbles', 'Injector', 'name:injector')


def test_should_add_injection_suffix_to_strings_in_a_json(injector_scenario: BubbleScenarioBuilder):
    mutations = (
        injector_scenario
        .when_input_request(a_json_request({'key': ['v1', 2, {'k2': 'hey'}]}))
    )
    assert len(mutations) == 2
    assert any(mutation for mutation in mutations if mutation.json_body == {'key': ["v1'`t;--/*", 2, {'k2': 'hey'}]})
    assert any(mutation for mutation in mutations if mutation.json_body == {'key': ['v1', 2, {'k2': "hey'`t;--/*"}]})


@form_urlencoded_or_query
def test_should_add_injection_suffix_to_urlencoded_parameters(
    injector_scenario: BubbleScenarioBuilder, request_with_params, params_in_mutant
):
    mutations = (
        injector_scenario
        .when_input_request(request_with_params(('query', 'string')))
    )
    assert len(mutations) == 1
    assert params_in_mutant(mutations[0]) == {'query': ["string'`t;--/*"]}


@form_urlencoded_or_query
def test_should_add_injection_suffix_to_repeated_urlencoded_parameters_separately(
    injector_scenario: BubbleScenarioBuilder, request_with_params, params_in_mutant
):
    mutations = (
        injector_scenario
        .when_input_request(request_with_params(('query', 'v1'), ('query', 'v2')))
    )
    assert len(mutations) == 2
    assert any(mutation for mutation in mutations if params_in_mutant(mutation) == {'query': ["v1'`t;--/*", "v2"]})
    assert any(mutation for mutation in mutations if params_in_mutant(mutation) == {'query': ["v1", "v2'`t;--/*"]})


@form_urlencoded_or_query
def test_should_add_injection_suffix_to_every_urlencoded_parameters_separately(
    injector_scenario: BubbleScenarioBuilder, request_with_params, params_in_mutant
):
    mutations = (
        injector_scenario
        .when_input_request(request_with_params(('q1', 'v1'), ('q2', 'v2')))
    )
    assert len(mutations) == 2
    assert any(mutation for mutation in mutations if params_in_mutant(mutation) == {'q1': ["v1'`t;--/*"], 'q2': ['v2']})
    assert any(mutation for mutation in mutations if params_in_mutant(mutation) == {'q1': ['v1'], 'q2': ["v2'`t;--/*"]})
