import inspect
import os
import sys
root_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
sys.path.insert(0, root_dir)

from fizzgun.models import HttpRequest
from fizzgun.models import HttpRequestBuilder
from fizzgun.models import Expectations
from fizzgun.models.expectation import Matcher


pages = [
    (HttpRequest,
     "When [creating your own bubbles](bubble-creation.md) the argument to the `generate` and `does_apply` methods is "
     "an instance of an `HttpRequest`."),
    (HttpRequestBuilder, None),
    (Expectations, None),
    (Matcher, None),
]


def main():
    sections = [gen_doc_for_class(klass, intro) for klass, intro in pages]
    with open(os.path.join(root_dir, 'docs', 'utility_classes.md'), 'w') as fh:
        fh.write("\n\n".join(sections))


def doc_section(filename, intro, klass):
    doc = gen_doc_for_class(klass)
    content = "%s\n\n%s" % (intro, doc)
    with open(filename, 'w') as fh:
        fh.write(content)


def gen_doc_for_class(klass, intro):
    class_methods = []
    properties = []
    methods = []

    doc = describe_class(klass, intro)

    for name in sorted(klass.__dict__.keys()):
        if name.startswith('_'):
            continue
        value = klass.__dict__[name]
        if isinstance(value, property):
            properties.append(describe_property(value))
        elif inspect.isfunction(value):
            methods.append(describe_function(value))
        elif isinstance(value, classmethod):
            class_methods.append(describe_function(value.__func__))

    if class_methods:
        doc += "\n%s" % gen_section("Class methods", class_methods)

    if properties:
        doc += "\n%s" % gen_section("Properties", properties)

    if methods:
        doc += "\n%s" % gen_section("Methods", methods)

    return doc


def gen_section(title, entries):
    doc = "**%s**:\n\n" % title
    for entry in entries:
        doc += "  * %s\n" % entry
    return doc


def get_type_name(annotation):
    if hasattr(annotation, '__name__'):
        return annotation.__name__
    return str(annotation).lstrip('typing.')


def get_return_type_name(annotations):
    if 'return' not in annotations:
        return 'None'
    return get_type_name(annotations['return'])


def get_doc(obj):
    return (obj.__doc__ or '').strip()


def describe_class(klass, intro):
    if intro:
        doc = "%s\n\n%s" % (intro, get_doc(klass))
    else:
        doc = get_doc(klass)
    return "### %s\n\n %s\n" % (klass.__name__, doc)


def describe_function(f):
    args = []
    annotations = f.__annotations__
    for var in f.__code__.co_varnames:
        if var in annotations:
            args.append("**%s**: *%s*" % (var, get_type_name(annotations[var])))
    args = ", ".join(args)

    return "**%s**(%s) → *%s* : %s" % (f.__name__, args, get_return_type_name(annotations), get_doc(f))


def describe_property(prop):
    f = prop.fget
    return "**%s →** *%s* : %s" % (f.__name__, get_return_type_name(f.__annotations__), get_doc(f))


if __name__ == '__main__':
    main()
