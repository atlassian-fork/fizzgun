# Configuration settings

You can find out what Fizzgun's default configuration settings are by running `fizzgun gen-config --defaults`, then open
the generated `fizzgun.yaml` file:

```yaml
bubbles:
  tags-blacklist: []
  tags-whitelist: []

  default-settings:
    expected_status_range: '0-499'
    mark_requests: false

  bubble-packs:
  - module: fizzgun.bubbles


filters: []

report:
  directory: ./reports
  format: txt

stack:
  queue_type: zmq
  use_processes: true
  transformers: 2
  requestors: 4


source: mitmproxy

mitmproxy:
  http_connect_ignore: null
  insecure: true
  port: 8888
  track_sessions: true
```

Let's go through each of these settings.

### Bubbles

These settings control which bubbles get loaded ([built-in](bubbles.md) and/or [custom](bubble-creation.md)) and how
they are initialized.

 * **tags-blacklist**: Bubbles are annotated with tags, some tags are shared by many bubbles while others are unique,
    via this setting you can prevent bubbles from being loaded by defining which tags you want to blacklist. To find
    out what tags each bubble has see the [bubbles command](commands.md#command-bubbles).
    
    For example, if you don't want the Shellshock Bubble to be loaded or any bubble tagged with `custom-tag`, you can
    define this blacklist:

```yaml
bubbles:
 tags-blacklist:
   - 'name:shellshock'
   - 'custom-tag'

...
```

 * **tags-whitelist**: Similarly, you can explicitly indicate which bubbles should be loaded by adding the appropriate
    tags to this list (however, in this case an empty list means that everything is whitelisted). Also if a bubble
    contains a tag that is blacklisted it won't be loaded even if it also has a whitelisted tag.
    
    For example, to load all the bubbles with the `category:data-validation` tag plus the shellshock bubble:

```yaml
bubbles:
 tags-whitelist:
   - 'name:shellshock'
   - 'category:data-validation'

...
```

 * **default-settings**: This is a mapping of key-value properties that are passed to each of the bubbles during
    initialization (the value can be of any type), you can override any of these for an individual bubble as it is
    described later in this section. The meaning of `mark_requests` and `expected_status_range` is explained in the
    [built-in bubbles](bubbles.md) section.


 * **bubble-packs**: These settings allow you to load your own bubbles, and also configure individual bubbles (built-in
    or custom) by extending or overwriting the `default-settings` we mentioned above. This configuration section is
    defined as a list of objects containing two properties: `module` and the optional `settings`. We'll expand more on
    these two on the [creating your own bubbles](bubble-creation.md) section. The `settings` property is an object where
    keys are bubble names, and values configuration objects similar to the `default-settings` object described above
    
    For instance lets see how you can configure the `grow_factor` setting of the 
    [Enlarger bubble](bubbles.md#bubble-enlarger) and overwrite the global `mark_requests` setting just for the
    [TypeChanger bubble](bubbles.md#bubble-typechanger):
    
```yaml
bubbles:

  bubble-packs:
  - module: fizzgun.bubbles # include the built-in bubble pack
    settings:
      Enlarger:
        grow_factor: 100
      TypeChanger:
        mark_requests: true

...
```

### Filters

By default Fizzgun will try to fuzz every request sample it gets, however, in some occasions you may want to just fuzz
only requests destined to certain endpoint or a specific path. The `filters` section of the configuration file defines
a list of rules specifying what should get fuzzed (whitelist). However, this list being empty (default) means
every request gets fuzzed.


Each entry in this list consist of a mapping with two properties, `endpoint` and `matchers`:
  
  * **endpoint** (mandatory): Fizzgun will process the list of `matchers` (or accept the request if no `matchers` are
  defined)  if the target `host:port` tuple of the request matches the value of this property. The value can represent
  an exact match (e.g. `www.google.com:443`) a regexp (e.g. `.+\.google\..+`) or `'*'` (match all the requests). Note
  that a request's target `host:port` could match none or many of these endpoint rules.
  
  * **matchers** (optional): zero or more fine grained conditions. If one or more are defined all of them must evaluate
   to true for the request to be accepted. Currently supported matchers include:
    * `path`: matches the URL path (without querystring), the value can be an exact match, a regex, or `*`
    * `path_with_query`: matches the URL path including querystring, the value can be an exact match, a regex, or '*'
    * `method`: matches the request's HTTP method, the value can be either a single method string, or a list of methods
    * `unseen`: matches if a request with the same signature has not been fuzzed already, the value is a list of one or
    more request attributes that compose the signature: `method`, `scheme`, `host`, `port`, `path`, `query`

**Summarizing**: endpoints apply in an **OR** fashion while matches in a given endpoint apply in an **AND** fashion.
  I.e. `accept request R if (R matches endpoint1 AND R matches endpoint1.matchers) OR (R matches endpoint2 AND ..)`
  

*Note*: regex are already assumed to match the whole string, so you don't have to include the `^`, `$` codes to match
the beginning/end of a string.

Let's go through some examples to clarify all this:

 * Only fuzz requests destined to any sub-domain of `example.com` and ports 80 or 443:

```yaml
filters:
  - endpoint: '.*\.example\.com:(80|443)'
```

 * The same as before but also fuzz any PUT request regardless what the host:port is:

```yaml
filters:
  - endpoint: '.*\.example\.com:(80|443)'

  - endpoint: '*'
    matchers:
      method: PUT
```

 * Only fuzz POST, PUT, or PATCH requests destined to `localhost:4567`:

```yaml
filters:
  - endpoint: 'localhost:4567'
    matchers:
      method: [POST, PUT, PATCH]
```

 * Fuzz *read-only* requests to any endpoint only if the path starts with `/rest/` or `/api/` and no request with the same
`host`, `port`, `path`, and `query` has been fuzzed already. Also allow fuzzing all POST requests to `/rest/login`:

```yaml
filters:
  - endpoint: '*'
    matchers:
      path: '/(rest|api)/.*'
      method: [GET, HEAD, TRACE]
      unseen: [host, port, path, query]
      
  - endpoint: '*'
    matchers:
      path: '/rest/login'
      method: POST
```


### Report

The `report` configuration section is composed of two properties:

 * **directory**: Filesystem directory where the report files should be created, if the directory does not exist Fizzgun
 will attempt to create it. Keep in mind that a relative path such as `./reports` is considered to be relative to the
 directory from which the `fizzgun run` command is executed (and not relative to the location of the config file).
 
 * **format**: either `txt` or `html`.
 
 
### Stack

The `stack` section of the configuration allows you to tweak some inner workings of Fizzgun that may help improving the
performance depending on your use case scenario. Internally, Fizzgun is composed of different entities that perform
individual tasks such as: filtering requests, mutating requests, sending mutated requests, verifying response
expectations, or reporting.

 * **queue_type**: Those Fizzgun entities communicate via queues, via this setting you can specify what queue technology
 to use, possible values are `zmq` to use [ZeroMQ](http://zeromq.org), or `python` to use
 [python's Queue](https://docs.python.org/3/library/queue.html#queue.Queue).
 
 * **use_processes**: When `true` Fizzgun entities will be executed in different processes to take advantage of
 multicore processors. Otherwise, the entities will be run in different threads within the same process.

 * **transformers**: The transformer is the entity in charge of invoking every bubble with a sample request and collect
 all the generated mutations. If you're rapidly feeding Fizzgun with lots of requests and if there are lots of bubbles
 loaded, the transformer could become a bottleneck. This property allows you to set the number of transformers that
 should be spawned to help balancing the load.

 * **requestors**: The requestor is the entity that receives a mutated request and sends it to the target server. Keep
 in mind that for every request that Fizzgun receives, hundreds of mutants could be generated. This property allows you
 to set the number of requestors that should be spawned to help balancing the load.
 

### Source

This defines the source of requests used to feed Fizzgun, at the moment the only value supported is `mitmproxy`.

### Mitmproxy

This section provides different settings to configure the `mitmproxy` source:

 * **http_connect_ignore**: A regex matcher to avoid interception of HTTPS requests based on the HTTP CONNECT
 `host:port` tuple. Usually you will want to filter out anything that is not your service under test. E.g.:
  `http_connect_ignore: '^(?!myservice\.com)(?!myservice\.org)'`. No filter is set when the value is `null`
 * **insecure**: If `true` mitmproxy is allowed to connect to HTTPS servers with invalid certificates.
 * **port**: Defines the port on which the HTTP proxy will be listening on.
 * **track_sesssions**: If `true` enables authentication in the proxy, any username/password is accepted but the
 username is used as a fizzgun session identifier, and it will be used to compose the name of the report file generated
 by Fizzgun. This allows you to use a single instance of Fizzgun for multiple different simultaneous users and keep all
 their reported results separated.
