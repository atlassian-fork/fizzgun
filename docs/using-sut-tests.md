
A simple way to feed Fizzgun with sample requests is to use existing automated integration tests such as api-tests or
even UI tests (e.g. Webdriver). Or basically any automated script that performs HTTP requests to the system under test.

The only change you need to do, is to configure those tests to route traffic through the HTTP proxy started by Fizzgun.

The following examples assume Fizzgun is running with the default [mitmproxy configuration](configuration.md#mitmproxy).

I.e. the proxy listens on `localhost:8888` and it requires authentication to enable session tracking (the `user` is used
as session-id and the `password` can be anything). If you disable session tracking (`track_sesssions: false`) you can
omit the authentication settings of the examples.


### General scenario

In most of the cases it should be enough to pass the `HTTP_PROXY` and/or `HTTPS_PROXY` environment variables to the
process running your tests or scripts. For example, if you are using `pytest` in a Python project, you can just run:

```text
HTTP_PROXY=http://session-name:pwd@localhost:8888 pytest [args...]
```

However, in some other cases the `HTTP_PROXY/HTTPS_PROXY` environment variables are not honored. Instead you need to
configure the proxy differently. Below there are some guides for cases like Java, Node JS, and Webdriver.


### Java

The JVM's global proxy settings are set via the following system properties:

  * `http.proxyHost`, `http.proxyPort`, `http.proxyUser`, `http.proxyPassword`
  * `https.proxyHost`, `https.proxyPort`, `https.proxyUser`, `https.proxyPassword`

E.g.:

```text
JAVA_PROXY=-Dhttp.proxyHost=localhost -Dhttp.proxyPort=8888 -Dhttp.proxyUser=session-name -Dhttp.proxyPassword=any
java ${JAVA_PROXY} your-command
```

For Maven or Gradle you might need to set these properties in your `pom.xml` or `gradle.properties` files.


### Node JS

At the moment of writing, Node's `http` and `https` modules 
[do not support global HTTP proxy settings](https://github.com/nodejs/node/issues/15620), i.e. it does not honour the
`HTTP_PROXY` or `HTTPS_PROXY` environment variables.

Some 3rd party libraries, like the popular [request](https://www.npmjs.com/package/request) library do honour global
proxy settings. So if the http client in your integration tests use `request` or any library with global proxy support,
all you need to do is set the `HTTP_PROXY/HTTPS_PROXY` env vars pointing to Fizzgun.

Otherwise, you might use [proxy-agent-patch](https://www.npmjs.com/package/proxy-agent-patch). Once installed include
the following before your code that perform requests:

```javascript
require('proxy-agent-patch')();
```

That will make node use the proxy settings from the environment. You can also specify them programmatically:

```javascript
require('proxy-agent-patch')({
  httpProxy: 'http://session-name:pwd@localhost:8888',
  httpsProxy: 'https://session-name:pwd@localhost:8888'
});
```


### Selenium Webdriver

For webdriver you need to configure the proxy settings on the browsers via capabilities. We are not going to describe
here how to do that since it varies depending on the programming language and browser used. You can refer to the
official [proxy configuration documentation](http://www.seleniumhq.org/docs/04_webdriver_advanced.jsp#using-a-proxy).
